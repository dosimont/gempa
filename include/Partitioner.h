/*********************************************************************************/
/*  Copyright 2022 Ricard Borrell Pol                                            */
/*                                                                               */
/*  This file is part of the GemPa library.                                      */
/*                                                                               */
/*  GemPa is free software: you can redistribute it and/or modify                */
/*  it under the terms of the GNU Lesser General Public License as published by  */
/*  the Free Software Foundation, either version 2.1 of the License, or          */
/*  (at your option) any later version.                                          */
/*                                                                               */
/*  GemPa is distributed in the hope that it will be useful,                     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*  GNU Lesser General Public License for more details.                          */
/*                                                                               */
/*  You should have received a copy of the GNU Lesser General Public License     */
/*  along with GemPa.  If not, see <https://www.gnu.org/licenses/>.              */
/*********************************************************************************/

#ifndef Partitioner_H
#define Partitioner_H
#define use_mpi 1
#if use_mpi
#include "mpi.h"
#endif
#include <iostream>
#include <vector>
#include <chrono>

using namespace std;

namespace gempa
{

  class Partitioner
  {

  public:
    /**
     * @brief Construct a new Partitioner object
     */
    Partitioner();
    /**
     * @brief Destroy the Partitioner object
     */
    ~Partitioner();

    /**
     * @brief Get the Total Weight of the geometric elements
     * @return total weight
     */
    double getTotWeight() const;

    /**
     * @brief Get the SFC partition Intervals
     * Generates a vector of dimension 2*npart + 1 storing for each
     * partition the minimum and maximum SFC incex
     * @return vector of partition intervals
     */
    vector<unsigned long long int> getIntervals();

    /**
     * @brief Print timing information for the given rank
     * Note: as there are many shincronization points variations
     * between ranks should be minimal.
     * @param rank
     */
    void printTimes(int rank = 0) const;

    /**
     * @brief: Generates the partition
     * In a parallel execution performa collective MPI comunicatoin.
     */
    void part();

    /**
     * @brief Checks the load balance once the partition is evaluated.
     * In a parallel execution performa collective MPI comunicatoin.
     * @return Load balance
     */
    double checkBalance();

    /**
     * @brief Check that SFC 1D slices are consecutive and do not intersect
     * In a parallel execution performa collective MPI comunicatoin, calls
     * function getIntervals()
     * @return true, ordered SFC slices
     * @return false, non-ordered SFC slices
     */
    bool checkOrdering();

    /********************************/
    // input
    int npart;
    int N;
    int sdim;
    vector<double> coords;
    vector<int> weights;
    bool isWeighted;
#if use_mpi
    MPI_Comm parComm;
#endif
    bool isParallel;

    // advanced input
    int iter;
    int maxIter;
    double targLB;
    bool doCorr;
    vector<double> corr;
    double relTol;
    double absTol;
    unsigned long long int maxIdx;
    bool printLB;

    // output
    vector<int> partition;
    vector<unsigned long long int> sfcIdx;

    // timers
    std::chrono::steady_clock::time_point begin_part;
    std::chrono::steady_clock::time_point end_Bb;
    std::chrono::steady_clock::time_point end_SFC;
    std::chrono::steady_clock::time_point end_Hist;

  private:
    void evalBboxAndWeights();
    void evalSFCIndexes();
    void histogramIterate();
    unsigned long long int linear_inverse(unsigned long long int x1, double y1, unsigned long long int x2,
                                          double y2, double y3, double minWidth);

    double minCoord[3];
    double maxCoord[3];
    double maxWeight;
    double avePartWeight;
    double maxPartWeight;
    double totWeight;
    double objLB;

    int parSize;
    int parRank;
  };

  //
  // INLINES
  //

  inline double Partitioner::getTotWeight() const
  {
    return totWeight;
  }
}
#endif
