/*********************************************************************************/
/*  Copyright 2022 Ricard Borrell Pol                                            */
/*                                                                               */
/*  This file is part of the GemPa library.                                      */
/*                                                                               */
/*  GemPa is free software: you can redistribute it and/or modify                */
/*  it under the terms of the GNU Lesser General Public License as published by  */
/*  the Free Software Foundation, either version 2.1 of the License, or          */
/*  (at your option) any later version.                                          */
/*                                                                               */
/*  GemPa is distributed in the hope that it will be useful,                     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*  GNU Lesser General Public License for more details.                          */
/*                                                                               */
/*  You should have received a copy of the GNU Lesser General Public License     */
/*  along with GemPa.  If not, see <https://www.gnu.org/licenses/>.              */
/*********************************************************************************/

#ifndef SFCORD_H
#define SFCORD_H

#include <iostream>
#include <vector>

using namespace std;

namespace gempa
{

  class HilbertSFC
  {

  public:
    /**
     * @brief Construct a new Hilbert SFC object
     */
    HilbertSFC();

    /**
     * @brief Destroy the Hilbert SFC object
     */
    ~HilbertSFC();

    /**
     * @brief Eval SFC coordinates 2D
     * @param x coordinate
     * @param y coordinate
     * @return SFC index
     */
    unsigned long long int evalIndex(double x, double y) const;

    /**
     * @brief  Eval SFC coordinates 3D
     * @param x coordinate
     * @param y coordinate
     * @param z coordinate
     * @return SFC index
     */
    unsigned long long int evalIndex(double x, double y, double z) const;

    /**
     * @brief Eval SFC coordinate 2D and 3D
     * @param coord coordianates array
     * @param sdim  space dimension
     * @return unsigned long long int
     */
    unsigned long long int evalIndex(double *coord, int sdim) const;

    // inputs
    double minCoord[3];
    double maxCoord[3];
    int maxLev;
  };
} // namespace gempa

#endif
