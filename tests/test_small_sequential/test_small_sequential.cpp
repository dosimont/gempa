/*********************************************************************************/
/*  Copyright 2022 Ricard Borrell Pol                                            */
/*                                                                               */
/*  This file is part of the GemPa library.                                      */
/*                                                                               */
/*  GemPa is free software: you can redistribute it and/or modify                */
/*  it under the terms of the GNU Lesser General Public License as published by  */
/*  the Free Software Foundation, either version 2.1 of the License, or          */
/*  (at your option) any later version.                                          */
/*                                                                               */
/*  GemPa is distributed in the hope that it will be useful,                     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*  GNU Lesser General Public License for more details.                          */
/*                                                                               */
/*  You should have received a copy of the GNU Lesser General Public License     */
/*  along with GemPa.  If not, see <https://www.gnu.org/licenses/>.              */
/*********************************************************************************/

#include "Partitioner.h"
#include <iostream>
#include <mpi.h>
#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{

  MPI_Init(&argc, &argv);
  int myRank, mySize;
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &mySize);

  gempa::Partitioner P1;

  //
  // Input data
  //
  P1.sdim = 2;
  P1.npart = 3;
  P1.isParallel = false;
  P1.isWeighted = true;
  P1.doCorr = false;
  P1.printLB = true;
  P1.maxIter = 100;
  P1.targLB = 0.80;

  P1.N = 11;

  P1.coords.resize(P1.N * P1.sdim);
  P1.coords[0] = 0.0;
  P1.coords[1] = 0.0;
  P1.coords[2] = 5.0;
  P1.coords[3] = 1.0;
  P1.coords[4] = 3.0;
  P1.coords[5] = 3.0;
  P1.coords[6] = 7.0;
  P1.coords[7] = 3.0;
  P1.coords[8] = 1.0;
  P1.coords[9] = 5.0;
  P1.coords[10] = 5.0;
  P1.coords[11] = 5.0;
  P1.coords[12] = 5.0;
  P1.coords[13] = 7.0;
  P1.coords[14] = 0.0;
  P1.coords[15] = 8.0;
  P1.coords[16] = 8.0;
  P1.coords[17] = 8.0;
  P1.coords[18] = 1.0;
  P1.coords[19] = 1.0;
  P1.coords[20] = 7.0;
  P1.coords[21] = 7.0;

  P1.weights.resize(P1.N);
  P1.weights[0] = 1;
  P1.weights[1] = 3;
  P1.weights[2] = 2;
  P1.weights[3] = 1;
  P1.weights[4] = 1;
  P1.weights[5] = 4;
  P1.weights[6] = 3;
  P1.weights[7] = 1;
  P1.weights[8] = 1;
  P1.weights[9] = 10;
  P1.weights[10] = 3;

  P1.part();

  ofstream result("partition.txt");
  for (int i = 0; i < P1.N; i++)
  {
    result << P1.partition[i] << endl;
  }
  result.close();

  MPI_Finalize();
  
  return 0;
}
