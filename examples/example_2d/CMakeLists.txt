cmake_minimum_required(VERSION 3.1)

project(example_2d)

#Add source and header files
file(GLOB_RECURSE SRC_FILES ${CMAKE_CURRENT_LIST_DIR}/*.cpp)
include_directories(${gempa_include_dir})

#Add sources
add_executable(${PROJECT_NAME} ${SRC_FILES})

#Add mpi include files and library to current project
find_package(PkgConfig REQUIRED)
find_package(MPI REQUIRED)
if (NOT MPI_CXX_FOUND)
  message(FATAL_ERROR "MPI not found!")
endif()

if(NOT TARGET MPI::MPI_CXX)
  add_library(MPI::MPI_CXX IMPORTED INTERFACE)
  set_property(TARGET MPI::MPI_CXX
               PROPERTY INTERFACE_COMPILE_OPTIONS ${MPI_CXX_COMPILE_FLAGS})
  set_property(TARGET MPI::MPI_CXX
               PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${MPI_CXX_INCLUDE_PATH}")
  set_property(TARGET MPI::MPI_CXX
               PROPERTY INTERFACE_LINK_LIBRARIES ${MPI_CXX_LINK_FLAGS} ${MPI_CXX_LIBRARIES})
endif()

target_link_libraries(${PROJECT_NAME} MPI::MPI_CXX)
set_target_properties(${PROJECT_NAME} PROPERTIES POSITION_INDEPENDENT_CODE ON)

#Link to gempa
target_link_libraries(${PROJECT_NAME} gempa)
