/*********************************************************************************/
/*  Copyright 2022 Ricard Borrell Pol                                            */
/*                                                                               */
/*  This file is part of the GemPa library.                                      */
/*                                                                               */
/*  GemPa is free software: you can redistribute it and/or modify                */
/*  it under the terms of the GNU Lesser General Public License as published by  */
/*  the Free Software Foundation, either version 2.1 of the License, or          */
/*  (at your option) any later version.                                          */
/*                                                                               */
/*  GemPa is distributed in the hope that it will be useful,                     */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of               */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                */
/*  GNU Lesser General Public License for more details.                          */
/*                                                                               */
/*  You should have received a copy of the GNU Lesser General Public License     */
/*  along with GemPa.  If not, see <https://www.gnu.org/licenses/>.              */
/*********************************************************************************/

#include <Partitioner.h>
#include <iostream>
#include <mpi.h>

using namespace std;

int main(int argc, char *argv[])
{

  MPI_Init(&argc, &argv);
  int myRank, mySize;
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &mySize);

  if (myRank == 0)
    cout << "First GeMPa test!" << endl;

  gempa::Partitioner P1;

  //
  // Input data
  //
  P1.sdim = 2;
  P1.npart = mySize;
  P1.isParallel = true;
  P1.isWeighted = true;
  P1.doCorr = false;
  P1.printLB = true;

  if (myRank == 0)
  {

    P1.N = 4;
    P1.coords.resize(P1.N * P1.sdim);
    P1.coords[0] = 0.0;
    P1.coords[1] = 0.0;
    P1.coords[2] = 5.0;
    P1.coords[3] = 1.0;
    P1.coords[4] = 3.0;
    P1.coords[5] = 3.0;
    P1.coords[6] = 7.0;
    P1.coords[7] = 3.0;

    P1.weights.resize(P1.N);
    P1.weights[0] = 1;
    P1.weights[1] = 3;
    P1.weights[2] = 2;
    P1.weights[3] = 1;
  }
  if (myRank == 1)
  {

    P1.N = 5;
    P1.coords.resize(P1.N * P1.sdim);
    P1.coords[0] = 1.0;
    P1.coords[1] = 5.0;
    P1.coords[2] = 5.0;
    P1.coords[3] = 5.0;
    P1.coords[4] = 5.0;
    P1.coords[5] = 7.0;
    P1.coords[6] = 0.0;
    P1.coords[7] = 8.0;
    P1.coords[8] = 8.0;
    P1.coords[9] = 8.0;

    P1.weights.resize(P1.N);
    P1.weights[0] = 1;
    P1.weights[1] = 4;
    P1.weights[2] = 3;
    P1.weights[3] = 1;
    P1.weights[4] = 1;
  }
  if (myRank == 2)
  {

    P1.N = 2;
    P1.coords.resize(P1.N * P1.sdim);
    P1.coords[0] = 1.0;
    P1.coords[1] = 1.0;
    P1.coords[2] = 7.0;
    P1.coords[3] = 7.0;

    P1.weights.resize(P1.N);
    P1.weights[0] = 10;
    P1.weights[1] = 3;
  }
  P1.part();
  if (myRank == 0)
  {
    cout << "Partitions: " << P1.npart << endl;
    cout << "Total weight: " << P1.getTotWeight() << endl;
  }
  MPI_Barrier(MPI_COMM_WORLD);
  cout << "Partition " << myRank << ": ";
  for (int i = 0; i < P1.N; ++i)
    cout << P1.partition[i] << " - ";
  cout << endl;

  cout << "Load balance: " << P1.checkBalance() << endl;
  cout << "Isordred: " << P1.checkOrdering() << endl;

  MPI_Finalize();
  return 0;
}
